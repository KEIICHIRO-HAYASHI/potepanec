require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { taxonomy.root }
  let(:product) { create(:product, taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario '商品詳細ページで商品名やそれに関連する情報が表示されること' do
    expect(page).to have_current_path potepan_product_path(product.id)
    expect(page).to have_title product.name
    expect(page).to have_selector ".page-title h2", text: product.name
    expect(page).to have_selector ".active", text: product.name
    expect(page).to have_selector ".media-body h2", text: product.name
    expect(page).to have_selector ".media-body h3", text: product.display_price
    expect(page).to have_selector ".media-body p", text: product.description
  end

  scenario '一覧ページへ戻るをクリックしたら商品一覧のページへ移動すること' do
    click_on "一覧ページへ戻る"
    visit potepan_category_path(product.taxons.first.id)
  end
end
