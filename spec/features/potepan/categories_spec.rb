require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  given(:taxonomy) { create(:taxonomy, name: "categories") }
  given(:taxon) { taxonomy.root }
  given!(:product) { create(:product, name: "Bags", taxons: [taxon]) }
  given(:shirts) { create(:taxon, name: "shirts") }
  
  background do
    visit potepan_category_path(taxon.id)
  end

  scenario 'タイトルがカテゴリー名になっていること' do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
  end

  scenario '商品の情報ページ' do
    within '.taxon_product_box' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.images.first
      expect(page).not_to have_content shirts.name
    end
  end

  scenario 'CategoriesまたはBrandをクリックしたらカテゴリー一覧が表示されること' do
    within '.category_nav' do
      find('.collapse_toggle').click
      taxon.leaves.each do |category|
        expext(page).to have_content category.id
        expext(page).to have_content category.name
        expext(page).to have_content category.products.count
      end
    end
  end

  scenario '商品をクリックしたら商品詳細ページへ移動すること' do
    click_on product.name
    visit potepan_product_path(product.id)
  end
end
