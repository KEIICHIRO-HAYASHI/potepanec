require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { taxonomy.root }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path product.id
    end

    it "products/showに正常にアクセスすることを確認する" do
      expect(response).to be_success
    end

    it "@productが定義されていることを確認する" do
      expect(controller.instance_variable_get('@product'))
    end

    it "画像が表示されている" do
      product.images.each do |image|
        expect(response.body).to include image.attachment(:product)
        expect(response.body).to include image.attachment(:small)
      end
    end
  end
end
