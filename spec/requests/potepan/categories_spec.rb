require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /show" do
    let(:brand_taxonomy) { create(:taxonomy, name: "brand") }
    let(:categories_taxonomy) { create(:taxonomy, name: "categories") }
    let(:taxon) { create(:taxon, taxonomy: categories_taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    
    before do
      get potepan_category_path taxon.id
    end

    it "categories/showに正常にアクセスすること" do
      expect(response).to be_success
    end

    it "@taxonが定義されている" do
      expect(controller.instance_variable_get('@taxon'))
    end

    it "@taxonomiesが定義されている" do
      expect(controller.instance_variable_get('@taxonomies'))
    end

    it "@taxon_productsが取得できている" do
      expect(controller.instance_variable_get('@taxon_products'))
    end

    it "taxonとtaxonomyのデータが表示されていること" do
      expect(response.body).to include categories_taxonomy.name
      expect(response.body).to include brand_taxonomy.name
      expect(response.body).to include taxon.name
    end
  end
end
