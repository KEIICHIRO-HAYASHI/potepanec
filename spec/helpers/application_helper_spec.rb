require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  it 'タイトルが商品名になっていること' do
    expect(full_title("page_title")).to eq "page_title - BIGBAG Store"
  end

  it '商品を選択していない時、タイトルがBIGBAG Storeのみ表示されること' do
    expect(full_title(nil)).to eq "BIGBAG Store"
  end
end
